export const environment = {
  production: true,
  trainingEndpoint: 'api/training',
  userEndpoint: 'api/user'
};
